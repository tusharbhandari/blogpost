<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HomeTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testHomePageIsWorkingCorrectly()
    {
        $response = $this->get('/');
        //$response->assertStatus(200);
        $response->assertSeeText('Home');
        $response->assertSeeText('This is home page content');
    }

    public function testContactPageIsWorkingCorrectly()
    {
        $response = $this->get('/contact');
        //$response->assertStatus(200);
        $response->assertSeeText('Contact');
        $response->assertSeeText('This is contact page content');
    }
}
