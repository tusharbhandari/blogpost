@extends('layout')
@section('content')
    <h1>{{ __('messages.welcome') }}</h1>
    <h1>@lang('messages.welcome')</h1>

    <p>{{ __('messages.example_with_value',['name' => 'Jhon'] ) }}</p>

    <p>{{ trans_choice('messages.plural', 0,['a' => 1]) }}</p>
    <p>{{ trans_choice('messages.plural', 1,['a' => 1]) }}</p>
    <p>{{ trans_choice('messages.plural', 2,['a' => 1]) }}</p>

    <h3>Using json</h3>
    <p>{{ __('Welcome to laravel!')}}</p>
    <p>{{ __('Hello :name',['name' => 'Tushar'])}}</p>
    <p>This is home page content</p>
@endsection