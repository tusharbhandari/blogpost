@extends('layout')
@section('content')
    <h1>Contact</h1>
    <p>This is contact page content</p>

    @can('home.secret')
        <a href="{{ route('secret') }}">
            Go to special contact Details!
        </a>

    @endcan
@endsection
