<div class="container">
    <div class="row">
        <div class="row mt-4">
            @card(['title' => 'Most Commented'])
                @slot('subtitle')
                    What people are currently talking about
                @endslot
                @slot('items')
                    @foreach ($mostCommented as $post)
                        <a href="{{ route('posts.show', ['post'=> $post->id]) }}">
                            <li class="list-group-item">{{ $post->title }}</li>
                        </a>
                    @endforeach
                @endslot
            @endcard
        </div>
    </div>

    <div class="row mt-4">
        @card(['title' => 'Most Active'])
            @slot('subtitle')
                Users With Most Post written
            @endslot
            @slot('items', collect($mostActive)->pluck('name'))
        @endcard
    </div>

    <div class="row mt-4">
        @card(['title' => 'Most Active Last Month'])
            @slot('subtitle')
                Users With Most Post written in the last month
            @endslot
            @slot('items', collect($mostActiveLastMonth)->pluck('name'))
        @endcard
    </div>
</div>