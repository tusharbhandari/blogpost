<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Auther extends Model
{
    public function profile()
    {
        return $this->hasOne('App\Profile');
    }
}
