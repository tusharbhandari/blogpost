<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    public function auther()
    {
        return $this->belongsTo('App\Auther');
    }
}
